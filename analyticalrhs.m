function [R_ac,R_au,R_av,u,v,p]= analyticalrhs(x,y,N)

u=zeros(N);
v=zeros(N);
p=zeros(N);
dudx=zeros(N);
dudy=zeros(N);
dvdx=zeros(N);
dvdy=zeros(N);
dudxdx=zeros(N);
dudydy=zeros(N);
dvdxdx=zeros(N);
dvdydy=zeros(N);
dpdx=zeros(N);
dpdy=zeros(N);

t=0;
Re=1;
for j=1:N
    for i=1:N
        u(j,i) = -exp(-2*t)*cos(x(j,i))*sin(y(j,i));
        v(j,i) = exp(-2*t)*cos(y(j,i))*sin(x(j,i));
        p(j,i) = -0.25*exp(-4*t)*(cos(2*x(j,i))+cos(2*y(j,i)));
        dudx(j,i) = exp(-2*t)*sin(x(j,i))*sin(y(j,i));
        dudy(j,i) = -exp(-2*t)*cos(x(j,i))*cos(y(j,i));
        dudxdx(j,i) = exp(-2*t)*cos(x(j,i))*sin(y(j,i));
        dudydy(j,i) = exp(-2*t)*cos(x(j,i))*sin(y(j,i));
        dvdx(j,i) = exp(-2*t)*cos(x(j,i))*cos(y(j,i));
        dvdy(j,i) = -exp(-2*t)*sin(x(j,i))*sin(y(j,i));
        dvdxdx(j,i) = -exp(-2*t)*cos(y(j,i))*sin(x(j,i));
        dvdydy(j,i) = -exp(-2*t)*cos(y(j,i))*sin(x(j,i));
        dpdx(j,i) =  0.5*exp(-4*t)*sin(2*x(j,i));
        dpdy(j,i) =  0.5*exp(-4*t)*sin(2*y(j,i));
    end
end



R_ac = zeros(N);
R_au = zeros(N);
R_av = zeros(N);

for j=1:N
    for i=1:N
        R_ac(j,i) = -(dudx(j,i)+dvdy(j,i));
        R_au(j,i) = -2*u(j,i)*dudx(j,i) - dpdx(j,i) -u(j,i)*dvdy(j,i)-v(j,i)*dudy(j,i)  + (1/Re)*(dudxdx(j,i)) + (1/Re)*(dudydy(j,i));
        R_av(j,i) = - u(j,i)*dvdx(j,i) - v(j,i)*dudx(j,i) -2*v(j,i)*dvdy(j,i) - dpdy(j,i) + (1/Re)*(dvdxdx(j,i)) + (1/Re)*(dvdydy(j,i));
    end
end

 contour(x,y,R_ac)
    
end
