function [R_ca,R_cu,R_cv] = curvilinearrhs(x,y,N,xita,eta,G,J,xita_x, xita_y,eta_x,eta_y,u,v,p)


%#codegen
coder.inline('never')

for m=1:3
    RHS=zeros(N,N,m);
end

R_ca = zeros(N);
R_cu = zeros(N);
R_cv = zeros(N);
[dEs1,dEs2,dis_1,dis_2,dis_3]= convection(x,y,N,xita,eta,G,J,xita_x, xita_y,eta_x,eta_y,u,v,p);



[dEv1,dEv2]= viscous(x,y,N,xita,eta,G,J,xita_x, xita_y,eta_x,eta_y,u,v,p);



     for j=2:N-1
        for i=2:N-1
    RHS(j,i,1) = (-dEs1(j,i,1)-dEs2(j,i,1) + dEv1(j,i,1)+dEv2(j,i,1) - dis_1(j,i))*J(j,i);
    RHS(j,i,2) = (-dEs1(j,i,2)-dEs2(j,i,2) + dEv1(j,i,2)+dEv2(j,i,2)- dis_2(j,i))*J(j,i);
    RHS(j,i,3) = (-dEs1(j,i,3)-dEs2(j,i,3) + dEv1(j,i,3)+dEv2(j,i,3)- dis_3(j,i))*J(j,i);
    
    R_ca(j,i)=RHS(j,i,1);
    R_cu(j,i)=RHS(j,i,2);
    R_cv(j,i)=RHS(j,i,3);
        end
     end
end


        