function [p1,u1,v1] = bcsuv(N)
p1=zeros(N);
u1=zeros(N);
v1=zeros(N);

for j=N
    for i=1:N
        u1(j,i)=1;
        v1(j,i)=0;
    end
end

for j=1
    for i=1:N
        u1(j,i)=0;
        v1(j,i)=0;
    end
end

for j=1:N-1
    for i=1
        u1(j,i)=0;
        v1(j,i)=0;
    end
end

for j=1:N-1
    for i=N
        u1(j,i)=0;
        v1(j,i)=0;
    end
end