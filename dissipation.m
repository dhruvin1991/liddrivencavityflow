function [dis_1,dis_2,dis_3]= dissipation(N,U,V,p,u,v,g_11,g_22,J)

dis_1=zeros(N);
dis_2=zeros(N);
dis_3=zeros(N);


episilon=0.001;


for j=1:N
    for i=1:N
        rho_a1(j,i) = ((abs(U(j,i)) + sqrt(U(j,i)^2 + g_11(j,i))))/J(j,i);
        rho_a2(j,i) = ((abs(V(j,i)) + sqrt(V(j,i)^2 + g_22(j,i))))/J(j,i);
    end
end

for j=1:N
    for i=2:N-2
        D_1(j,i,1)= episilon*rho_a1(j,i)*(p(j,i+2)-3*p(j,i+1)+3*p(j,i)-p(j,i-1));
        D_1(j,i,2)= episilon*rho_a1(j,i)*(u(j,i+2)-3*u(j,i+1)+3*u(j,i)-u(j,i-1));
        D_1(j,i,3)= episilon*rho_a1(j,i)*(v(j,i+2)-3*v(j,i+1)+3*v(j,i)-v(j,i-1));
    end
end

for j=1:N
    for i=N-1
        D_1(j,i,1)=D_1(j,N-2,1) ;
        D_1(j,i,2)= D_1(j,N-2,2);
        D_1(j,i,3)= D_1(j,N-2,3);
    end
end

for j=2:N-2
    for i=1:N
        D_2(j,i,1)= episilon*rho_a2(j,i)*(p(j+2,i)-3*p(j+1,i)+3*p(j,i)-p(j-1,i));
        D_2(j,i,2)= episilon*rho_a2(j,i)*(u(j+2,i)-3*u(j+1,i)+3*u(j,i)-u(j-1,i));
        D_2(j,i,3)= episilon*rho_a2(j,i)*(v(j+2,i)-3*v(j+1,i)+3*v(j,i)-v(j-1,i));
    end
end

for j=N-1
    for i=1:N
        D_2(j,i,1)= D_2(N-2,i,1);
        D_2(j,i,2)= D_2(N-2,i,1) ;
        D_2(j,i,3)= D_2(N-2,i,1);
    end
end
for m=1:3
    for j=1:N
        for i=2:N-2
            D_xita(j,i,m) =  D_1(j,i,m) - D_1(j,i-1,m);
        end
    end
end

for m=1:3
    for j=2:N-2
        for i=1:N
            D_eta(j,i,m) =  D_2(j,i,m) - D_2(j-1,i,m);
        end
    end
end


for j=2:N-2
    for i=2:N-2
        dis_1(j,i) = D_xita(j,i,1)+ D_eta(j,i,1);
        dis_2(j,i) = D_xita(j,i,2) + D_eta(j,i,2);
        dis_3(j,i) = D_xita(j,i,3) + D_eta(j,i,3);
    end
end
    
