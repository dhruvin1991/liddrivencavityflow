clear all; close all; clc;

r=[1];
N=[121];
delta_t=0.001;
alpha=[1/4,1/3,1/2,1];
for l=1:length(r)
    a=0;
    
  
    for k=1:length(N)
        a=a+1;
        
%         [x,y,dx]=gridgeneration(N(k),r(l));
%         [p_old,u_old,v_old] = ibcs(x,y,N(k));
%         dx2(k) = dx;
%         [xita,eta,G,J,xita_x, xita_y,eta_x,eta_y,x_xita,y_eta] = metrics(x,y,N(k));
%         [R_ca,R_cu,R_cv] = curvilinearrhs(x,y,N(k),xita,eta,G,J,xita_x, xita_y,eta_x,eta_y,u_old,v_old,p_old);
          [x,y,dx]=gridgeneration(N(k),r(l));
        
        [p_old,u_old,v_old] = ibcs(x,y,N(k));
        [pnew,unew,vnew] = ibcs(x,y,N(k));
        dx2(k) = dx;
        
        [xita,eta,G,J,xita_x, xita_y,eta_x,eta_y,x_xita,y_eta] = metrics(x,y,N(k));
        
        
        [R_ca,R_cu,R_cv] = curvilinearrhs_mex(x,y,N(k),xita,eta,G,J,xita_x, xita_y,eta_x,eta_y,u_old,v_old,p_old);
        
        a=0;
         for t=delta_t:delta_t:60000
             a=a+1;
             [p1,u1,v1] = bcsuv(N(k));    
             for j=2:N(k)-1
                 for i=2:N(k)-1
                    
                     p1(j,i) = p_old(j,i) + alpha(1)*delta_t*R_ca(j,i);
                     u1(j,i) = u_old(j,i) + alpha(1)*delta_t*R_cu(j,i);
                     v1(j,i) = v_old(j,i) + alpha(1)*delta_t*R_cv(j,i);   
                 end
             end
             [p1] = bcsp(p1,N(k));
         
         [R_ca1,R_cu1,R_cv1] = curvilinearrhs_mex(x,y,N(k),xita,eta,G,J,xita_x, xita_y,eta_x,eta_y,u1,v1,p1);
          [p2,u2,v2] = bcsuv(N(k));    
             for j=2:N(k)-1
                 for i=2:N(k)-1
                    
                     p2(j,i) = p_old(j,i) + alpha(2)*delta_t*R_ca1(j,i);
                     u2(j,i) = u_old(j,i) + alpha(2)*delta_t*R_cu1(j,i);
                     v2(j,i) = v_old(j,i) + alpha(2)*delta_t*R_cv1(j,i);   
                 end
             end
             [p2] = bcsp(p2,N(k));
         
         [R_ca2,R_cu2,R_cv2] = curvilinearrhs_mex(x,y,N(k),xita,eta,G,J,xita_x, xita_y,eta_x,eta_y,u2,v2,p2);
         [p3,u3,v3] = bcsuv(N(k));    
             for j=2:N(k)-1
                 for i=2:N(k)-1
                    
                     p3(j,i) = p_old(j,i) + alpha(3)*delta_t*R_ca2(j,i);
                     u3(j,i) = u_old(j,i) + alpha(3)*delta_t*R_cu2(j,i);
                     v3(j,i) = v_old(j,i) + alpha(3)*delta_t*R_cv2(j,i);   
                 end
             end
             [p3] = bcsp(p3,N(k));
         
         [R_ca3,R_cu3,R_cv3] = curvilinearrhs_mex(x,y,N(k),xita,eta,G,J,xita_x, xita_y,eta_x,eta_y,u3,v3,p3);
         
         [pnew,unew,vnew] = bcsuv(N(k));    
             for j=2:N(k)-1
                 for i=2:N(k)-1
                    
                     pnew(j,i) = p_old(j,i) + alpha(4)*delta_t*(R_ca3(j,i));
                     unew(j,i) = u_old(j,i) + alpha(4)*delta_t*(R_cu3(j,i));
                     vnew(j,i) = v_old(j,i) + alpha(4)*delta_t*(R_cv3(j,i));   
                 end
             end
             [pnew] = bcsp(pnew,N(k));
             
             for j=2:N(k)-1
                 for i=2:N(k)-1
                     
                      err(j,i) = (pnew(j,i)-p_old(j,i))^2+(unew(j,i)-u_old(j,i))^2+(vnew(j,i)-v_old(j,i))^2;

                 end
             end
             h=N(k);
             error(a) = sqrt(sum(sum(err))/((h)^2));
             
             if(error(a) < 10^-7)
                            disp('solution converged');
                            for j=1:N(k)
                                for i=1:N(k)
                                    ans1(j,i,a) = pnew(j,i);
                                    ans2(j,i,a) = unew(j,i);
                                    ans3(j,i,a) = vnew(j,i);
                                end
                            end
                            figure;streamslice(x,y,unew,vnew);title('streamlines for uniform grid for Re=1000');xlabel('x');ylabel('y');print('streamlinesunigi1000','-dpng')
                            
                         
                         break;
             end
             u_old= unew;
             p_old = pnew;
             v_old = vnew;
 
         
         end
        
    end
end

% %plotting
% figure;plot(error);title('Error V/s iteration for uniform grid at Re=1000');xlabel('iteration');ylabel('Error');print('errunigr1000','-dpng')
%  Nm = N/2 + 0.5;
%  figure;plot(unew(:,Nm),y,'r');title('u velocity at geometric center for uniform grid at Re=1000');xlabel('u');ylabel('y');print('uvelstrec400','-dpng')
%  figure;plot(y,vnew(Nm,:),'r');title('v velocity at geometric center for uniform grid at Re=1000');xlabel('x');ylabel('v');print('vvelstrec400','-dpng')
%  [vort,~] = curl(x,y,unew,vnew);  
%  figure;contour(x,y,vort,500);title('vorticity for uniform grid at Re=1000');xlabel('x');ylabel('y');print('vortsrec400','-dpng')

% 



