function [xita,eta,G,J,xita_x, xita_y,eta_x,eta_y,x_xita,y_eta] = metrics(x,y,N)

%generate xita, eta mesh

delta_xita=1;
xi = zeros(1,N);
for z=2:N
    xi(z)=xi(z-1)+delta_xita;
end
eta1=xi;

xita=zeros(1,N);
eta=zeros(1,N);

for j=1:N;
    for i=1:N;
        xita(j,i)=xi(i);
        eta(j,i)=eta1(j);
    end
end

 x_xita=zeros(N);
 x_eta=zeros(N);
 y_xita=zeros(N);
 y_eta=zeros(N);
 
 xita_x=zeros(N);
 eta_x=zeros(N);
 xita_y=zeros(N);
 eta_y=zeros(N);
 
 G=zeros(N);
 J=zeros(N);
 
for j=2:N-1;
    for i=2:N-1;
    x_xita(j,i) = 0.5*(x(j,i+1)-x(j,i-1));
    x_eta(j,i) = 0.5*(x(j+1,i)-x(j-1,i));
    y_xita(j,i) = 0.5*(y(j,i+1)-y(j,i-1));
    y_eta(j,i) = 0.5*(y(j+1,i)-y(j-1,i));
    end
end

%all x_xita
%along x only
%middle
for j=1
    for i=2:N-1
    x_xita(j,i)= 0.5*(x(j,i+1)-x(j,i-1));
    end
end
%start
for j=1
    for i=1
    x_xita(j,i)= x(j,i+1)-x(j,i);
    end
end
%end
for j=1
    for i=N
    x_xita(j,i)= x(j,i)-x(j,i-1);
    end
end

%middle
for j=N
    for i=2:N-1
    x_xita(j,i)= 0.5*(x(j,i+1)-x(j,i-1));
    end
end

%start
for j=N
    for i=1
    x_xita(j,i)= x(j,i+1)-x(j,i);
    end
end

%end
for j=N
    for i=N
    x_xita(j,i)= x(j,i)-x(j,i-1);
    end
end

%along y

for j=2:N-1
    for i=1
    x_xita(j,i)= x(j,i+1)-x(j,i);
    end
end

for j=2:N-1
    for i=N
    x_xita(j,i)= x(j,i)-x(j,i-1);
    end
end


%all y_eta
%along y only
%middle
for i=1
    for j=2:N-1
    y_eta(j,i)= 0.5*(y(j+1,i)-y(j-1,i));
    end
end
%start
for i=1
    for j=1
    y_eta(j,i)= y(j+1,i)-y(j,i);
    end
end
%end
for i=1
    for j=N
    y_eta(j,i)= y(j,i)-y(j-1,i);
    end
end

%middle
for i=N
    for j=2:N-1
    y_eta(j,i)= 0.5*(y(j+1,i)-y(j-1,i));
    end
end

%start
for i=N
    for j=1
    y_eta(j,i)= y(j+1,i)-y(j,i);
    end
end

%end
for j=N
    for i=N
    y_eta(j,i)= y(j,i)-y(j-1,i);
    end
end

%along x

for i=2:N-1
    for j=1
    y_eta(j,i)= y(j+1,i)-y(j,i);
    end
end

for i=2:N-1
    for j=N
    y_eta(j,i)= y(j,i)-y(j-1,i);
    end
end

%all x_eta
%along x only
%middle
for i=1
    for j=2:N-1
    x_eta(j,i)= 0.5*(x(j+1,i)-x(j-1,i));
    end
end
%start
for i=1
    for j=1
    x_eta(j,i)= x(j+1,i)-x(j,i);
    end
end
%end
for i=1
    for j=N
    x_eta(j,i)= x(j,i)-x(j-1,i);
    end
end

%middle
for i=N
    for j=2:N-1
    x_eta(j,i)= 0.5*(x(j+1,i)-x(j-1,i));
    end
end

%start
for i=N
    for j=1
    x_eta(j,i)= x(j+1,i)-x(j,i);
    end
end

%end
for j=N
    for i=N
    x_eta(j,i)= x(j,i)-x(j-1,i);
    end
end

%along y

for i=2:N-1
    for j=1
    x_eta(j,i)= x(j+1,i)-x(j,i);
    end
end

for i=2:N-1
    for j=N
    x_eta(j,i)= x(j,i)-x(j-1,i);
    end
end

%all y_xita
%along x only
%middle
for j=1
    for i=2:N-1
    y_xita(j,i)= 0.5*(y(j,i+1)-y(j,i-1));
    end
end
%start
for j=1
    for i=1
    y_xita(j,i)= y(j,i+1)-y(j,i);
    end
end
%end
for j=1
    for i=N
    y_xita(j,i)= y(j,i)-y(j,i-1);
    end
end

%middle
for j=N
    for i=2:N-1
    y_xita(j,i)= 0.5*(y(j,i+1)-y(j,i-1));
    end
end

%start
for j=N
    for i=1
    y_xita(j,i)= y(j,i+1)-y(j,i);
    end
end

%end
for j=N
    for i=N
    y_xita(j,i)= y(j,i)-y(j,i-1);
    end
end

%along y

for j=2:N-1
    for i=1
    y_xita(j,i)= y(j,i+1)-y(j,i);
    end
end

for j=2:N-1
    for i=N
    y_xita(j,i)= y(j,i)-y(j,i-1);
    end
end

 for j=1:N
     for i=1:N
         G(j,i) = x_xita(j,i)*y_eta(j,i)-x_eta(j,i)*y_xita(j,i);
     end
 end
 J=1./G;
  for j=1:N;
    for i=1:N;
    xita_x(j,i) = y_eta(j,i)/G(j,i);
    eta_x(j,i) = -y_xita(j,i)/G(j,i);
    xita_y(j,i) = -x_eta(j,i)/G(j,i);
    eta_y(j,i) = x_xita(j,i)/G(j,i);
    end
  end
 
%  for j=1:N
%      for i=1:N
%          J(j,i) = xita_x(j,i)*eta_y(j,i)-eta_x(j,i)*xita_y(j,i);
%      end
%  end
%   J=1./G;
