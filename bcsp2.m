function [p1] = bcsp2(p1,N);
Nm=N/2+0.5;
for j=N
    for i=1:N
        p1(j,i)=p1(j-1,i);
        
    end
end

for j=1
    for i=1:N
         p1(j,i)=p1(j+1,i);
      
    end
end

for j=1:N-1
    for i=1
        p1(j,i)=p1(j,i+1); 
    end
end

for j=1:N-1
    for i=N
        p1(j,i)=p1(j,i-1);
    end
end 

p_ref = p1(Nm,Nm);
for j=1:N
    for i=1:N
        p1(j,i) = p1(j,i) - p_ref;
    end
end