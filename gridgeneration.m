function [x y dx1] = gridgeneration(N,r)

   Nm=N/2+0.5;
   Ne=N-Nm;
   x1 = zeros(1,N);       
        
    if r==1
        dx=1/(N-1);
        x1=0:dx:1;
      else
       % Generate streched geomtric grid
          a= (0.5*1*(1-r))/(1-r^(Nm-1));
          x1(2)=a;
          for i=3:Nm
             x1(i)=x1(i-1)+a*r^(i-2);
          end
           b= (-0.5*1*(1-r))/(1-r^(Ne-1));
            x1(N)=1;
          x1(N-1)=1+b;
          c=2;
          for i=2:Ne-1
              c=c+1;
             x1(N-i)=x1(N-i+1)+b*r^(i-2);
          end

%            x1(N)=pi;
%             for i=1:Ne-1;
%                 x1(N-i)=pi - x1(i+1);
%             end
      end

 y1=x1;

x=zeros(N);
y=zeros(N);
for j=1:N
    for i=1:N
        x(j,i)=x1(i);
        y(j,i)=y1(j);
    end
end
dx1= x1(2)-x1(1);