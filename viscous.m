function [dEv1,dEv2]= viscous(x,y,N,xita,eta,G,J,xita_x, xita_y,eta_x,eta_y,u,v,p)


for m=1:3
        Ev1=zeros(N,N,m);
        Ev2=zeros(N,N,m);
        dEv1=zeros(N,N,m);
        dEv2=zeros(N,N,m);
end


Re=1000;

Jhp=zeros(N);

for j=1:N
    for i=1:N-1
        Jhp(j,i) = 0.5*(J(j,i+1)+J(j,i));
    end
end
for j=1:N
    for i=N
        Jhp(j,i) = 0.5*(J(j,i)+J(j,i-1));
    end
end


xita_xhp=zeros(N);

for j=1:N
    for i=1:N-1
       xita_xhp(j,i)= 0.5*(xita_x(j,i+1)+xita_x(j,i));
    end
end

for j=1:N
    for i=N
       xita_xhp(j,i)= 0.5*(xita_x(j,i)+xita_x(j,i-1));
    end
end

xita_yhp=zeros(N);

for j=1:N
    for i=1:N-1
       xita_yhp(j,i)= 0.5*(xita_y(j,i+1)+xita_y(j,i));
    end
end

for j=1:N
    for i=N
       xita_yhp(j,i)= 0.5*(xita_y(j,i)+xita_y(j,i-1));
    end
end

eta_xhp=zeros(N);

for j=1:N
    for i=1:N-1
       eta_xhp(j,i)= 0.5*(eta_x(j,i+1)+eta_x(j,i));
    end
end

for j=1:N
    for i=N
       eta_xhp(j,i)= 0.5*(eta_x(j,i)+eta_x(j,i-1));
    end
end

eta_yhp=zeros(N);

for j=1:N
    for i=1:N-1
       eta_yhp(j,i)= 0.5*(eta_y(j,i+1)+eta_y(j,i));
    end
end

for j=1:N
    for i=N
       eta_yhp(j,i)= 0.5*(eta_y(j,i)+eta_y(j,i-1));
    end
end


g_11p=zeros(N);
g_12p =zeros(N);
g_22p =zeros(N);

for j=1:N
    for i=1:N
        g_11p(j,i) = (xita_xhp(j,i))^2 + (xita_yhp(j,i))^2;
        g_12p(j,i) = xita_xhp(j,i)*eta_xhp(j,i) + xita_yhp(j,i)*eta_yhp(j,i);
        g_22p(j,i) = (eta_xhp(j,i))^2 + (eta_yhp(j,i))^2;
    end
end



for j=2:N-1
    for i=1:N-1
        Ev1(j,i,1)=0;
        Ev1(j,i,2) = (g_11p(j,i)*(u(j,i+1)-u(j,i)) +  g_12p(j,i)*(u(j+1,i)-u(j,i)))/(Re*Jhp(j,i));
        Ev1(j,i,3) = (g_11p(j,i)*(v(j,i+1)-v(j,i)) +  g_12p(j,i)*(v(j+1,i)-v(j,i)))/(Re*Jhp(j,i));
    end
end

for j=2:N-1
    for i=2:N-1
        dEv1(j,i,2) = Ev1(j,i,2) - Ev1(j,i-1,2);
        dEv1(j,i,3) = Ev1(j,i,3) - Ev1(j,i-1,3);
    end
end

Jhn=zeros(N);

for j=1:N-1
    for i=1:N
        Jhn(j,i) = 0.5*(J(j+1,i)+J(j,i));
    end
end
for j=N
    for i=1:N
        Jhn(j,i) = 0.5*(J(j,i)+J(j-1,i));
    end
end


xita_xnp=zeros(N);

for j=1:N-1
    for i=1:N
       xita_xnp(j,i)= 0.5*(xita_x(j+1,i)+xita_x(j,i));
    end
end

for j=N
    for i=1:N
       xita_xnp(j,i)= 0.5*(xita_x(j,i)+xita_x(j-1,i));
    end
end

xita_yhn=zeros(N);

for j=1:N-1
    for i=1:N
       xita_yhn(j,i)= 0.5*(xita_y(j+1,i)+xita_y(j,i));
    end
end

for j=N
    for i=1:N
       xita_yhn(j,i)= 0.5*(xita_y(j,i)+xita_y(j-1,i));
    end
end

eta_xhn=zeros(N);

for j=1:N-1
    for i=1:N
       eta_xhn(j,i)= 0.5*(eta_x(j+1,i)+eta_x(j,i));
    end
end

for j=N
    for i=1:N
       eta_xhn(j,i)= 0.5*(eta_x(j,i)+eta_x(j-1,i));
    end
end

eta_yhn=zeros(N);

for j=1:N-1
    for i=1:N
       eta_yhn(j,i)= 0.5*(eta_y(j+1,i)+eta_y(j,i));
    end
end

for j=N
    for i=1:N
       eta_yhn(j,i)= 0.5*(eta_y(j,i)+eta_y(j-1,i));
    end
end


g_11n=zeros(N);
g_12n =zeros(N);
g_22n =zeros(N);

for j=1:N
    for i=1:N
        g_11n(j,i) = (xita_xnp(j,i))^2 + (xita_yhn(j,i))^2;
        g_12n(j,i) = xita_xnp(j,i)*eta_xhn(j,i) + xita_yhn(j,i)*eta_yhn(j,i);
        g_22n(j,i) = (eta_xhn(j,i))^2 + (eta_yhn(j,i))^2;
    end
end



for j=1:N-1
    for i=2:N-1
        Ev2(j,i,1)=0;
        Ev2(j,i,2) = (g_12n(j,i)*(u(j,i+1)-u(j,i)) +  g_22n(j,i)*(u(j+1,i)-u(j,i)))/((Re*Jhn(j,i)));
        Ev2(j,i,3) = (g_12n(j,i)*(v(j,i+1)-v(j,i)) +  g_22n(j,i)*(v(j+1,i)-v(j,i)))/((Re*Jhn(j,i)));
    end
end

for j=2:N-1
    for i=2:N-1
        dEv2(j,i,2) = Ev2(j,i,2) - Ev2(j-1,i,2);
        dEv2(j,i,3) = Ev2(j,i,3) - Ev2(j-1,i,3);
    end
end



