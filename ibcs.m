function [p,u,v]= ibcs(x,y,N)

u=zeros(N);
v=zeros(N);
p=zeros(N);

for j=N
    for i=1:N
        u(j,i)=1;
    end
end
