function [dEs1,dEs2,dis_1,dis_2,dis_3] = convection(x,y,N,xita,eta,G,J,xita_x, xita_y,eta_x,eta_y,u,v,p)

for m=1:3
    Es1=zeros(N,N,m);
    Es2=zeros(N,N,m);

    dEs1=zeros(N,N,m);
    dEs2=zeros(N,N,m);

    D_1=zeros(N,N,m);
    D_2=zeros(N,N,m);
    D_xita=zeros(N,N,m);
    D_eta=zeros(N,N,m);
end
U = zeros(N);
V = zeros(N);
rho_a1= zeros(N);
rho_a2= zeros(N);
for j=1:N
    for i=1:N
        U(j,i) = (u(j,i)*xita_x(j,i) + v(j,i)*xita_y(j,i));
    end
end
for j=1:N
    for i=1:N
         Es1(j,i,1) = U(j,i)/J(j,i);
    end
end

for j=1:N
    for i=1:N
        Es1(j,i,2) = (u(j,i)*U(j,i) + p(j,i)*xita_x(j,i))/J(j,i);
    end
end

for j=1:N
    for i=1:N
        Es1(j,i,3) = (v(j,i)*U(j,i) + p(j,i)*xita_y(j,i))/J(j,i);
    end
end

for j=1:N
    for i=1:N
        V(j,i) = (u(j,i)*eta_x(j,i) + v(j,i)*eta_y(j,i));
    end
end

for j=1:N
    for i=1:N
        Es2(j,i,1) = V(j,i)/J(j,i);
    end
end

for j=1:N
    for i=1:N
        Es2(j,i,2) = (u(j,i)*V(j,i) + p(j,i)*eta_x(j,i))/J(j,i);
    end
end

for j=1:N
    for i=1:N
        Es2(j,i,3) = (v(j,i)*V(j,i) + p(j,i)*eta_y(j,i))/J(j,i);
    end
end


for j=2:N-1
    for i=2:N-1
        dEs1(j,i,1)=(Es1(j,i+1,1)-Es1(j,i-1,1))/2;
        dEs1(j,i,2)=(Es1(j,i+1,2)-Es1(j,i-1,2))/2;
        dEs1(j,i,3)=(Es1(j,i+1,3)-Es1(j,i-1,3))/2;
    end
end

for j=2:N-1
    for i=2:N-1
        dEs2(j,i,1)=(Es2(j+1,i,1)-Es2(j-1,i,1))/2;
        dEs2(j,i,2)=(Es2(j+1,i,2)-Es2(j-1,i,2))/2;
        dEs2(j,i,3)=(Es2(j+1,i,3)-Es2(j-1,i,3))/2;
    end
end

g_11=zeros(N);
g_12 =zeros(N);
g_22 =zeros(N);

for j=1:N
    for i=1:N
        g_11(j,i) = (xita_x(j,i))^2 + (xita_y(j,i))^2;
        g_12(j,i) = xita_x(j,i)*eta_x(j,i) + xita_y(j,i)*eta_y(j,i);
        g_22(j,i) = (eta_x(j,i))^2 + (eta_y(j,i))^2;
    end
end

dis_1=zeros(N);
dis_2=zeros(N);
dis_3=zeros(N);


episilon=0.01;


for j=1:N
    for i=1:N
        rho_a1(j,i) = ((abs(U(j,i)) + sqrt(U(j,i)^2 + g_11(j,i))))/J(j,i);
        rho_a2(j,i) = ((abs(V(j,i)) + sqrt(V(j,i)^2 + g_22(j,i))))/J(j,i);
    end
end

for j=1:N
    for i=2:N-2
        D_1(j,i,1)= episilon*rho_a1(j,i)*(p(j,i+2)-3*p(j,i+1)+3*p(j,i)-p(j,i-1));
        D_1(j,i,2)= episilon*rho_a1(j,i)*(u(j,i+2)-3*u(j,i+1)+3*u(j,i)-u(j,i-1));
        D_1(j,i,3)= episilon*rho_a1(j,i)*(v(j,i+2)-3*v(j,i+1)+3*v(j,i)-v(j,i-1));
    end
end

for j=1:N
    for i=N-1
        D_1(j,i,1)=D_1(j,N-2,1) ;
        D_1(j,i,2)= D_1(j,N-2,2);
        D_1(j,i,3)= D_1(j,N-2,3);
    end
end

for j=2:N-2
    for i=1:N
        D_2(j,i,1)= episilon*rho_a2(j,i)*(p(j+2,i)-3*p(j+1,i)+3*p(j,i)-p(j-1,i));
        D_2(j,i,2)= episilon*rho_a2(j,i)*(u(j+2,i)-3*u(j+1,i)+3*u(j,i)-u(j-1,i));
        D_2(j,i,3)= episilon*rho_a2(j,i)*(v(j+2,i)-3*v(j+1,i)+3*v(j,i)-v(j-1,i));
    end
end

for j=N-1
    for i=1:N
        D_2(j,i,1)= D_2(N-2,i,1);
        D_2(j,i,2)= D_2(N-2,i,1) ;
        D_2(j,i,3)= D_2(N-2,i,1);
    end
end
for m=1:3
    for j=1:N
        for i=2:N-2
            D_xita(j,i,m) =  D_1(j,i,m) - D_1(j,i-1,m);
        end
    end
end

for m=1:3
    for j=2:N-2
        for i=1:N
            D_eta(j,i,m) =  D_2(j,i,m) - D_2(j-1,i,m);
        end
    end
end


for j=2:N-2
    for i=2:N-2
        dis_1(j,i) = D_xita(j,i,1)+ D_eta(j,i,1);
        dis_2(j,i) = D_xita(j,i,2) + D_eta(j,i,2);
        dis_3(j,i) = D_xita(j,i,3) + D_eta(j,i,3);
    end
end
    
